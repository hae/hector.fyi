---
title: How to contact me
---

For those who have not yet made my acquaintance, you can send me a quick email
at the address "me (at) myfullname.com" where "myfullname.com" is really the
second-level domain name of this website. This is only the public web email
address; I may reply from a different address, at another domain.

Whenever possible, please follow these best practices for sending me email:
+ Use plain text, not HTML format. It will be much easier for me to read with a
  text-based mail user agent, and take up less storage.
+ Encrypt messages to my OpenPGP [public key](/pubkey.asc) for enhanced security
  and privacy. The subject line will not be encrypted, so enter a random
  five-digit number as the subject if you're doing this.
+ Do not save the public web email address to your address book. Instead, I can
  send you a vCard file (VCF) with my full contact details, which you can then
  import into a contacts application or other database.

## Online profiles, to bug me further

+ [GitHub](https://github.com/HectorAE)
+ [GitLab](https://gitlab.com/hae)
+ [LinkedIn](https://www.linkedin.com/in/hector-escobedo)
