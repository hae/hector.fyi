+++
title = "Example draft blog post"
date = '2021-07-10T12:30:00-07:00'
draft = true
description = """
An example file for creating new blog posts and ensuring they're formatted \
consistently and properly.
"""
categories = [ "Example", "Draft" ]
tags = [
"Example",
"Draft",
"blog post",
"template",
]
+++

**DELETE EXAMPLE DRAFT BELOW**

Content goes here!

Remember to remove `draft` variable from front matter prior to publishing! Also
update the `date` and `lastmod` variables, and always keep the time zone offset.
In TOML, every datetime value should be wrapped in single quotation marks; this
helps nudge the timezone formatter to make it look pretty.

**DELETE EXAMPLE DRAFT ABOVE**
