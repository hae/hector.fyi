+++
title = "The benefits of having a dedicated writing area"
date = '2021-07-14T20:00:00-07:00'
description = """
Using a dedicated desk or area for handwriting in a notebook or journal can \
be conducive to maintaining a regular writing routine.
"""
categories = [ "Personal", "Writing" ]
tags = [ "Personal", "Writing", "handwriting", "journal", "home office" ]
+++

Have you ever found yourself wanting to write more, yet seemingly unable to find
a time or place to do so? I know I have, even when there wasn't really anything
stopping me from just opening up a notebook and scrawling away. The modality of
writing by hand is actually quite different from writing on digital devices. I
find both to be useful and necessary for different tasks and times of day. Here,
I will cover a seemingly minor change in organizing my personal space that's had
a major impact in helping me form the habit of writing in my daily journal.

## Why I began a daily journal

Bullet journaling is a fad I've had my eye on for a while. It seems promising, a
proven method to enhance your personal productivity and organize your life,
right? Well, although I've bought more than one fancy dot-grid notebook touted
by bullet journal enthusiasts, I must admit that the actual work to go through
and set up the journal system always seemed too much. So last year, I instead
decided to just start a simple weekly journal, recording what happened in my
life in order to later review it and order my thoughts. It would be handwritten,
because you can't hack paper, and also I needed to use all those fancy pens I
got to go with the fancy notebooks for _something_.

The problem was, I did not actually stick to my weekly schedule for writing down
all those events and thoughts every seven days. Somehow it always slipped, and I
told myself I'd do it the next day, and then the day after that, or I might even
forget. Working from home and having one day blur into another, over and over,
didn't help things. Large time gaps started forming between entries. Many
worthwhile things I could have recorded were undoubtedly lost in the
cracks. Well, as part of a broader shift in my personal life, namely trying to
establish better habits for myself and remain mindful of my schedule, I
eventually came up with the idea of writing on a daily basis instead of
weekly. The upshot is that in some cases, it can be easier to form a daily habit
because it fits in with your life's natural rhythms and tracking your progress
is relatively continuous. It just takes a certain amount of discipline to
actually start it and keep it going.

## A tidy writing space

In addition to the temporal aspect of a habit, our minds naturally associate
certain places and vistas with a set of behaviors and expectations. I was once
told by college advisors that you should keep your bed out of view from your
work desk, because we subconsciously associate it with sleep and rest rather
than grinding and focusing on work. Unfortunately, I've never had a room or
apartment big enough to keep my sleeping area completely out of sight from my
work area. Maybe one day! Regardless, I reasoned that if I was going to
successfully start a daily journal, I should have a dedicated space to do so.

I have two desks in my room currently; one for the desktop monitor and keyboard,
and another which had just kind of accumulated junk and where I kept my personal
laptop. The latter didn't really make sense because I am not really using my
laptop every day, and it's portable, so I can just stuff it in a backpack or on
a shelf when not in use. That's what I did, as well as clearing off the extra
clutter, and my smaller desk became a fully analog, personal reading and writing
area.

My journal notebook is always on the desk and ready to write in at a moment's
notice, along with a nice rollerball pen. Books I intend to read at some point
(in the near-to-mid-term future) go on one corner of the desk, and there's also
room for reference materials and extra writing and drawing implements. I found
that the one real issue was that I wasn't getting much good quality light to
write by, what with the desk being in a corner and only getting indirect natural
light. I didn't have any desk lamps, so I ended up using a small battery-powered
LED work light that I had in my closet, the kind jewelers or watch repairers
might use, and it proved satisfactory. I now have a dedicated functional space
that I can use with ergonomic ease for journaling, sketching, reading, or any
other kind of task involving pen or pencil and paper.

The results so far are promising: I've written in my journal every day for the
past ten days. Most days I even make multiple entries, in the morning,
afternoon, and evening. It's turned out to be quite positive for my general
mood; keeping daily logs of my thoughts and experiences helps me find a bit of
life balance and look beyond the immediate moment. Just writing a couple of
paragraphs per day also helps me to keep the frame of mind needed for textual
composition sharp, so that I can be more productive outside of journaling as
well. To any readers that seek to start some good journaling or general writing
habits, I greatly recommend taking care of the spatial requirements for your
routine and creating a dedicated area just for writing, even if it's small. The
simple act of tidying up your personal space can make all the difference.
